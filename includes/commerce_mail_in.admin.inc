<?php

/**
 * @file
 * Administrative forms for the Commerce Mail In Payment module.
 */

function commerce_mail_in_payment_types_overview() {

  $output = "";

  //Add Form
  $output = render(drupal_get_form("commerce_mail_in_payment_types_form"));

  //Payment Types table
  $header = array(t("Machine name"), t("Label"), t("Operations"));

  $result = db_select('commerce_mail_in_payment_type', 'c')->fields('c')->execute();
  $rows = array();
  while ($record = $result->fetchAssoc()) {
    $rows[] = array($record['name'], $record['label'], l(t("Delete"), "admin/commerce/config/payment-methods/manage/commerce_mail_in/payment_types/{$record['name']}/delete"));
  }

  $output .= theme("table", array('header' => $header, 'rows' => $rows));

  return $output;
}

function commerce_mail_in_payment_types_form(&$form_state) {
  $form = array();

  $form['payment_type'] = array(
    '#type' => 'fieldset',
    '#title' => t("Add a payment type"),
  );

  $form['payment_type']['label'] = array(
    '#type' => 'textfield',
    '#title' => t("Label"),
    '#required' => TRUE,
  );

  $form['payment_type']['name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'exists' => 'commerce_mail_in_payment_type_exists',
    ),
  );

  $form['payment_type']['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Add"),
  );

  return $form;
}

function commerce_mail_in_payment_types_form_submit($form, &$form_state) {
  $name = $form_state['values']['name'];
  $label = $form_state['values']['label'];

  db_insert('commerce_mail_in_payment_type')
    ->fields(array(
      'name' => $name,
      'label' => $label,
    ))
    ->execute();

  drupal_set_message(t("@label has been added to the payment type list.", array("@label" => $label)));
}

function commerce_mail_in_payment_type_exists($value) {
  // Query to check if payment type exists
  return db_query_range('SELECT 1 FROM {commerce_mail_in_payment_type} WHERE name = :menu', 0, 1, array(':menu' => $value))->fetchField();
}

function commerce_mail_in_payment_types_delete_form($form, &$form_state, $name) {
  $form = array();

  $result = db_select("commerce_mail_in_payment_type", "t")
              ->fields("t", array("label"))
              ->condition('name', $name, '=')
              ->execute()
              ->fetchAssoc();
  $label = $result['label'];

  $form['name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );

  $form['label'] = array(
    '#type' => 'value',
    '#value' => $label,
  );

  $message = t("Are you sure that you want to delete @label",  array("@label" => $label));

  $caption = t("This action cannot be undone.");

  return confirm_form($form, $message, "admin/commerce/config/payment-methods/manage/commerce_mail_in/payment_types", $caption, t('Delete'));
}

function commerce_mail_in_payment_types_delete_form_submit($form, &$form_state) {
  $name = $form_state['values']['name'];
  $label = $form_state['values']['label'];

  $delete = db_delete('commerce_mail_in_payment_type')
              ->condition('name', $name, "=")
              ->execute();

  drupal_set_message(t("@label has been deleted from the payment type list.", array("@label" => $label)));

  $form_state['redirect'] = "admin/commerce/config/payment-methods/manage/commerce_mail_in/payment_types";
}